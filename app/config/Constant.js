const Constant = {
    app_name:'EasyBuddy',
    APIPost: "POST",
    APIGET: "GET",
    APIImageUploadAndroid: "POST_UPLOAD",

    //Base_Url: 'https://easybuddy.in/api/auth/', // live
    Base_Url: 'https://easybuddy.in/stagging/api/auth/',  // testing


    //ImageBaseUrl:"ttp://easybuddy.in/public",
    ImageBaseUrl:"http://dilkhushpvtltd.com/easybuddy/public/",


   TermsAndCondition : "http://easybuddy.in/term-condition",
   privacy : "http://easybuddy.in/privacy",
   about :"http://easybuddy.in/about-us",
   contactUs : "http://easybuddy.in/contactus",
   faq:"https://easybuddy.in/faq",

    register :"register",
    login :"login",
  //  subCategory:"sub-category",
    category:"category",
    subcategory:"sub-category",
    services:"services",
    updateProfile :"update-profile",
    uploadImage :"update-image-profile",
    addAddress : "add-address",
    serviceDetails : "service-details",
    changePassword :"change-password",
    banner : "banner",
    listAddress :"list-address",
    deleteAddress :"delete-address",
    bookingList : "booking-list",
    saveBooking :"save-booking",
    timeSlotsList:"time-slots-list",
    daysSlotsList:"days-slots-list",
    bookingDetails :"booking-details",
    addToCart : "add-to-cart",
    cartLists : "cart-lists",
    quantityCounter  :"quantity-counter",
    couponList:"coupon-list",
    couponApply:"coupon-apply",
    products:"products",
    addtocartproduct:"add-to-cart-product",
    quantitycounterproduct:"quantity-counter-product",
    charges:"charges",
    cancelledBooking : "cancelled-booking",
    saveRatings:"save-ratings",
    raiseIssues:"raise-issues",
    forgotPassword:"forgot-password",
    notificationList :"notification-list",


    maxMobile : 15,
    maxNameEmail : 50,
    maxPass:15,
 
}
export default Constant;