import React from 'react';
import { Text, View, FlatList, TouchableOpacity, Alert, bookingComplete, StyleSheet, SafeAreaView, DeviceEventEmitter, } from 'react-native';
import styles from './BookingStyles';
import Colors from '../../Assets/Colors';
import fonts from '../../Assets/fonts';
import AppHeader from '../../Comman/AppHeader';
import RNPickerSelect from '../../Comman/CommonPicker'
import ApiCallHelper from '../../config/ApiCallHelper'
import Constant from '../../config/Constant'
import Helper from '../../config/Helper'
import moment from 'moment';

export default class Booking extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            arrBooking: [],
            arrFilter: [
                { label: 'All Booking', value: '' },
                { label: 'Pending', value: 'pending' },
                { label: 'Requested', value: 'requested' },
                { label: 'Assigned', value: 'assigned' },
                { label: 'Confirmed', value: 'confirmed' },
                { label: 'Completed', value: 'completed' },
                { label: 'Closed', value: 'closed' },
            ],
            filterId: '',
            apiResponce: false,

        }
        AppHeader({
            ...this.props.navigation, leftTitle: 'My Booking', borderBottomRadius: 0,
            bellIcon: false,
            settingsIcon: false,
            headerBg: false,
            hideLeftBackIcon: false,

        })
    }



    componentDidMount() {
        this.bookingComplete = DeviceEventEmitter.addListener("bookingComplete", (data) => {
            this.getBookings(this.state.filterId)
        })
        this.getBookings(this.state.filterId)
    }




    cancelBooking(val) {
        let data = {
            booking_id: val.booking_id
        }
        Helper.globalLoader.showLoader();
        ApiCallHelper.getNetworkResponce(Constant.cancelledBooking, JSON.stringify(data), Constant.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == true) {
                this.getBookings(this.state.filterId)
            } else {

            }
        }).catch(err => {
            Helper.globalLoader.hideLoader()
        })
    }

    getBookings(val) {
        this.setState({ filterId: val, arrBooking: [] })
        let data = {
            user_id: Helper.userData.id,
            type: val
        }
        Helper.globalLoader.showLoader();
        ApiCallHelper.getNetworkResponce(Constant.bookingList, JSON.stringify(data), Constant.APIPost).then((response) => {
            Helper.globalLoader.hideLoader();
            if (response.status == true) {
                this.setState({ arrBooking: response.data, })
            } else {

            }
        }).catch(err => {
            Helper.globalLoader.hideLoader()
        })
    }

    _renderUpComingItem = ({ item, index }) => {
        return (
            <View style={styles.history_view}>
                {index == 0 ? null :
                    <View style={{ height: 1, backgroundColor: 'gray', width: '90%', marginBottom: 15, marginTop: 5, marginHorizontal: 16 }}></View>
                }
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('BookingDetails', { data: item }) }}>
                    <View style={styles.img_calender_location_list_view}>
                        <Text style={{ fontFamily: fonts.PoppinsExtraBold, color: Colors.black, fontSize: 13, fontWeight: 'bold' }}>Booking ID: </Text>
                        <Text style={{ fontSize: 12, color: 'gray' }}>{item.booking_id}</Text>
                    </View>
                    <View style={styles.img_calender_location_list_view}>
                        <Text style={{ fontFamily: fonts.PoppinsExtraBold, color: Colors.black, fontSize: 13, fontWeight: 'bold' }}>Booking Date: </Text>
                        <Text style={{ fontSize: 12, color: 'gray' }}>{moment(item.created_at).format("DD-MM-YYYY")}</Text>
                    </View>
                    <View style={styles.img_calender_location_list_view}>
                        <Text style={{ fontFamily: fonts.PoppinsExtraBold, color: Colors.black, fontSize: 13, fontWeight: 'bold' }}>Request Date: </Text>
                        <Text style={{ fontSize: 12, color: 'gray' }}>{moment(item.delivery_date).format("DD-MM-YYYY")}</Text>
                    </View>
                    {console.log("cancelled----", item)}
                    {item.status == "cancelled" ?
                        <View style={styles.img_calender_location_list_view}>
                            <Text style={{ fontFamily: fonts.PoppinsExtraBold, color: Colors.black, fontSize: 13, fontWeight: 'bold' }}>Cancelled Date: </Text>
                            <Text style={{ fontSize: 12, color: 'gray' }}>{item?.cancelled_date}</Text>
                        </View> : null }

                    <View style={styles.img_calender_location_list_view}>
                        <Text style={{ fontFamily: fonts.PoppinsExtraBold, color: Colors.black, fontSize: 13, fontWeight: 'bold' }}>Status: </Text>
                        <Text style={{ flex: 1, fontSize: 12, color: item.status == "Cancelled" || item.status == "cancelled" ? "red" : 'gray' }}>{item.status}</Text>
                        {item.status == "completed" || item.status == "cancelled" || item.status == "Completed" || item.status == "Cancelled" ? null :
                            <TouchableOpacity onPress={() => this.CancelAlert(item)} style={{ backgroundColor: 'black', justifyContent: 'center', alignItems: 'center', borderRadius: 20, paddingHorizontal: 10 }}>
                                <Text style={{ color: Colors.white, fontSize: 12, fontFamily: fonts.PoppinsSemiBold, }}>Cancel</Text>
                            </TouchableOpacity>
                        }
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    CancelAlert(item) {
        Alert.alert(
            "EasyBuddy",
            "Are you sure want to Cancel Booking ?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => this.cancelBooking(item) }
            ]
        );
    }


    render() {
        return (
            <SafeAreaView style={styles.safe_area_view}>

                <View style={{ backgroundColor: '#F4EDED', marginTop: 10, marginHorizontal: 15, paddingHorizontal: 5 }}>
                    <RNPickerSelect
                        //label={LanguagesIndex.translate('LanguagePreference')}
                        items={this.state.arrFilter}
                        placeHolder={{}}

                        onValueChange={(value) => { this.getBookings(value) }}
                        selectValue={this.state.filterId}
                        useNativeAndroidPickerStyle={false}
                        style={pickerSelectStyles}
                    />
                </View>
                {this.state.arrBooking.length <= 0 ? <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center' }}>Service not available </Text>
                </View> :

                    <View style={{ marginHorizontal: 15, marginTop: 10, marginBottom: 70, elevation: 1, backgroundColor: Colors.white, borderRadius: 10, padding: 5 }} key="1">


                        <FlatList
                            showsVerticalScrollIndicator={false}
                            data={this.state.arrBooking}
                            renderItem={this._renderUpComingItem}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                        />

                    </View>
                }

            </SafeAreaView>
        )
    }

};

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 14,
        height: 40,
        color: '#000',
        width: '100%',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 10,
        fontFamily: fonts.PoppinsExtraBold,
        marginLeft: 10
    },
    inputAndroid: {
        fontSize: 14,
        height: 40,
        color: '#000',
        marginRight: 20,
        marginLeft: 8,
        marginBottom: 10,
        fontFamily: fonts.PoppinsExtraBold
    },
});
